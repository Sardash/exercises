import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by M155763 on 2017-07-26.
 */
public class Calculator {

    public static void main(String[] args) {
        String input = "3*((5-8)+45)*2+3*((16-(4+3)-27)+5)/4*3=";
        double endResult;

        System.out.println("1: " + input);


        while (input.matches(".*\\(.*\\).*")){
            input = solveParenthesis(input);
        }

        endResult = Double.parseDouble(calculateSection(input));
        System.out.println("End result: " + endResult);
    }

    public static String solveParenthesis (String input){
        Pattern pattern = Pattern.compile("(\\([^()]*\\))");
        Matcher matcher = pattern.matcher(input);
        matcher.find();

        String noParentheses = matcher.group(1).replaceAll("^\\(|\\)$", "");
        String result = matcher.replaceFirst(calculateSection(noParentheses));
        return result;
    }


    public static String calculateSection(String input){
        while (input.matches(".*(-?\\d+(\\.\\d+)?)[*/+\\-=](-?\\d+(\\.\\d+)?).*") || input.matches(".*(-?\\d+(\\.\\d+)?)=")){
            if (input.matches(".*\\*.*") || input.matches(".*/.*")){
                Pattern pattern = Pattern.compile("(-?\\d+(\\.\\d+)?)(\\*|/)(-?\\d+(\\.\\d+)?)");
                Matcher matcher = pattern.matcher(input);
                while (matcher.find()){
                    double result = -1;
                    if (matcher.group(3).contains("*")){
                        result = Double.parseDouble(matcher.group(1)) * Double.parseDouble(matcher.group(4));
                    }else if (matcher.group(3).contains("/")){
                        result = Double.parseDouble(matcher.group(1)) / Double.parseDouble(matcher.group(4));
                    }
                    input = matcher.replaceFirst(String.valueOf(result));
                    matcher = pattern.matcher(input);
                }
            }else if (input.matches(".*\\+.*") || input.matches(".+-.*")){
                Pattern pattern = Pattern.compile("(-?\\d+(\\.\\d+)?)(\\+|-)(-?\\d+(\\.\\d+)?)");
                Matcher matcher = pattern.matcher(input);
                while (matcher.find()){
                    double result = -1;
                    if (matcher.group(3).contains("+")){
                        result = Double.parseDouble(matcher.group(1)) + Double.parseDouble(matcher.group(4));
                    }else if (matcher.group(3).contains("-")){
                        result = Double.parseDouble(matcher.group(1)) - Double.parseDouble(matcher.group(4));
                    }
                    input = matcher.replaceFirst(String.valueOf(result));
                    matcher = pattern.matcher(input);
                }
            } else if (input.contains("=")){
                Pattern pattern = Pattern.compile("(\\d+(\\.\\d+)?)(=)");
                Matcher matcher = pattern.matcher(input);
                matcher.find();
                input = matcher.replaceFirst(matcher.group(1));
            }
        }
        return input;
    }

}


